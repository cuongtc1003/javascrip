console.log("hello");
//variable
var username = "Bod";
console.log(username);
console.log("username");

//datetype ~ kiểu dữ liệu
//string
var cat = "tom";

//number
var age = 3;

//boolean ~ true/false
var isLogin = true;

//null
// camel case
var isMarried = null;

// null:hiện không có nhưng có thể có
// undefinded:không có

//snake case
//undefided
var is_married = undefined;

var isHoliday = true;
console.log("isHoliday", isHoliday);
var isHoliday = false;
console.log("isHoliday", isHoliday);

// operater

var num1= 1;
var num2= 2;
var num3= 2 + 3;
var num4 = num4+1;
num4= num4 + num1;

//-
var num5=2*2;
//4
var num6 = 6/3;
//2
//% :chia lay du
var num7 =21%5;
console.log('num7');
console.log('num3');
var num8=5;
//num8 = num8+1;
num8 +=1;
//num8++
//++num8
var num9= ++num8 + 2;
console.log(num8);
console.log(num9);
var num10= 10;
var num11=num10++ +5;
