/*
baitap1:tính số ngày lương
input:
    +lương 1 ngày =100000
    +Ngày Làm =30

Bước 1: Tạo biến luong1Ngay, ngayLam
Bước 2: Gán giá trị cho luong1Ngay,soNgayLam, luong
Bước 3: Tính tổng lương với số tiền và ngày làm của nhân viên
Bước 4: In kết quả theo biểu mẫu ra console   

output:
    +Lương Nhân Viên = 3000000
*/
var Luong1ngay = 100000;
var ngayLam = 30;

var luong=0;

luong = Luong1ngay * ngayLam;
console.log("Lương nhân viên=",luong);

// bai tap 2

// Tính trung bình 5 số thực
/*
đầu vào:
    numBer1=20
    numBer2=25
    numBer3=30
    numBer4=35
    numBer5=40
Bước 1: Tạo biến numBer1, numBer2, numBer3, numBer4, numBer5
Bước 2: Gán giá trị cho numBer1, numBer2, numBer3, numBer4, numBer5
Bước 3: Tính giá trị của tổng 5 số người dùng nhập chia cho 5
Bước 4: In kết quả ra console


đầu ra:
    Giá trị trung bình = 30
*/

var numBer1 = 20;
var numBer2 = 25;
var numBer3 = 30;
var numBer4 = 35;
var numBer5 = 40;

giatritrungbinh = (numBer1 + numBer2 + numBer3 + numBer4 + numBer5)/5
console.log("Giá Trị Trung Bình =",giatritrungbinh);

//bài tập 3
// Tính và xuất ra số tiền quy đổi

/*
đầu vào:
    +giaVND =23500
    +soluongUSD =2

Bước 1: Tạo biến giaVND, soluongUSD, gia2USD
Bước 2: Gán giá trị cho giaVND, soluongUSD, gia2USD
Bước 3: Quy đổi số lượng USD sang VND 
Bước 4: In kết quả theo biểu mẫu ra console

đầu ra:
    +giá 2 USD = 47000
*/
var giaVND = 23500;
var soluongUSD = 2;

var gia2USD = 0;

gia2USD = giaVND * soluongUSD;
console.log("Giá 2 USD =",gia2USD);

//bài tập 4
// Tính diện tích hình chữ nhật
/*
Đầu vào:
    +chieuDai =10
    +chieuRong =5

Bước 1: Tạo biến chieuDai, chieuRong, chuVi, dienTich
Bước 2: Gán giá trị cho chieuDai, chieuRong
Bước 3: Tính công thức diện tích và chu vi
Bước 4: In 2 kết quả (chu vi và diện tích) ra console
   
Đầu ra:
    +chu vi = 30
    +dien tich = 50
*/

var chieuDai = 10;
var chieuRong = 5;

var chuVi=0;
var dienTich=0;

chuviCN = (chieuDai + chieuRong)*2;
dientichCN = chieuDai * chieuRong;

console.log("Chu vi hình chữ nhật là:",chuviCN);
console.log("Diện tích hình chữ nhật là:",dientichCN);

//bai 5
/*
Đầu vào:
        +n=23;
Đầu ra:
        +Tổng 2 ký số = 5
*/
var n = 23;

var tong2KySo = 0;

var hangChuc = Math.floor (n / 10);
var donVi = Math.floor (n % 10);

tong2KySo = hangChuc + donVi;

console.log('Tổng 2 ký số =', tong2KySo);
